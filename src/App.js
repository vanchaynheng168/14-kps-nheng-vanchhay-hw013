// import React from 'react';
// import './App.css';
import HomePage from './components/HomePage';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Menu from './components/Menu';
import GetAPI from './components/GetAPI';
import Add from './components/FormAdd';
import View from './components/View';
import Update from './components/FormUpdate';
import axios from "axios"

import React, { Component } from 'react'

export default class App extends Component {
  constructor(props){
    super(props);
    this.state ={
        users: [],
        title: "",
        description: "",
        image: ""
    };
        
}
  componentDidMount(){
    axios
    .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
    .then((res)=>{
        this.setState({
            users: res.data.DATA,
        });
    });
  }
  handleDelete = (id) => {
    axios
      .delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res)=>{
          alert(res.data.MESSAGE);
          this.componentDidMount();
      }); 
  }
  
  render() {
    return (
      <Router>
        <Menu/>
        {/* <GetAPI/> */}
          <Switch>
            {/* <Route path='/' exact component={GetAPI}/> */}
            <Route path= '/' exact render = {(props) => <GetAPI {...props} data = {this.state.users} handleDelete={this.handleDelete} />}/>
            {/* <Route path='/add' exact component={FormAdd}/> */}
            <Route path='/add' exact render = {(props) => <Add {...props}/>}/>
            <Route path= '/view/:id' render = {(props)=><View {...props} data = {this.state.users}/>}/>
            {/* <Route path= '/edit/:id' render = {(props)=><Update {...props} data = {this.state.users}/>}/> */}
            <Route path= '/edit/:id' component={Update}/>
          </Switch>
        
      </Router>
    )
  }
}

