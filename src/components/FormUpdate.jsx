
import { Container,Form,Button, Col,Image,Row } from 'react-bootstrap'
import axios from "axios"
import React, { Component } from 'react'

export default class FormUpdate extends Component {
    constructor(){
        super();
        this.state ={
            title: "",
            description: "",
            image: "",
            errorTitle: "" ,
            errorDescription: "" ,
            CheckTitle: false,
            CheckDecription: false
        }
    }
    handleUpdate = (id) => {
        const {title, description} = this.state
        let Article ={
            TITLE : title,
            DESCRIPTION: description
        };
        //PUT
        if(this.state.CheckTitle !==true ) {
            this.setState({
                errorTitle: " (title can't be empty.)",
            })
            if(this.state.CheckDecription !==true){
                this.setState({
                    errorDescription: "Des"
                })
            }
        }else{
            axios
            .put(`http://110.74.194.124:15011/v1/api/articles/${id}`,Article)
            .then((res)=>{
                alert(res.data.MESSAGE);
                // this.componentWillMount();
                window.history.back();
            });
        }
    }
    onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
          this.setState({
            image: URL.createObjectURL(event.target.files[0]),
          });
        }
      };
    
    componentWillMount(){
        const {match} = this.props
        let {id} = match.params;
        console.log(id);
        // Edit
        axios
        .get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
        .then((res)=>{
            let article = res.data.DATA;
            this.setState ({
                title : article.TITLE,
                description: article.DESCRIPTION
            });
            
        });
        console.log(this.state.title);
    }
    // onChange = (event) => {
    //     this.setState({[event.target.name]: event.target.value});
    // }
    handleInputTitleChage = (input) => {

        var title = input.target.value        
        var isTitleCorrect = 'false'
        if (title === "") {
            isTitleCorrect = false
            this.setState({
                title: title,
                CheckTitle: isTitleCorrect
            }) 
        }
        else {
            isTitleCorrect = true
            this.setState({
                errorTitle: '',
                title: title,
                CheckTitle: isTitleCorrect
            })   
        }
    }
    handleInputDescriptionChage = (input) => {
        var description = input.target.value        
        var isDes = 'false'
        if (description === "") {
            isDes = false
            this.setState({
                description: description,
                CheckDecription: isDes
            }) 
        }
        else {
            isDes = true
            this.setState({
                errorDescription: '',
                description: description,
                CheckDes: isDes
            })   
        }
    }
    render() {
        return (
            <Container>
            <h1>Add Article</h1>
            <Col md={12}>
                <Row>
                <Col md={8}>
                <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>TITLE</Form.Label>
                    <span id="errorTitle" style={{color : 'red'}}>{this.state.errorTitle}</span>
                    <Form.Control type="text" placeholder="Enter Title" name="title" value={this.state.title} onChange={this.handleInputTitleChage}/>
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <span id="Description" style={{color : 'red'}}>{this.state.errorDescription}</span>
                    <Form.Control type="text" placeholder="Enter Description" name="description" value={this.state.description} onChange={this.handleInputDescriptionChage}/>
                </Form.Group>
                <Button variant="primary" onClick={(id)=>this.handleUpdate(this.props.match.params.id)}>
                    Submit
                </Button>
                </Form>
            </Col>
            <Col md={4}>
            <div
            style={{ border: "1px solid #ccc" ,height: "150px", width : '200px'}}>
            <input
              type="file"
              style={{ opacity: "0", width: "100%" }}
              onChange={(e) => this.onImageChange(e)}
            //   className="filetype"
              id="group_image"
            />
            <img
              src={this.state.image}
              alt=""
              style={{ width: "100%", height: "88%" }}
            />
           </div>
            </Col>
                </Row>
            </Col>
        </Container>
        )
    }
}




