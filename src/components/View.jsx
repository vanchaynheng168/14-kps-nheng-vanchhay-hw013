import React from 'react'
import { Col, Row, Container } from 'react-bootstrap';

export default function View({match, data}) {
    
    var viewDetail = data.find((id)=> id.ID == match.params.id)
    return (
        <Container>
            <h1>Article</h1>
            <Col md={12}>
                <Row>
                    <Col md={3}>
                        <img src={viewDetail.IMAGE} alt="" style= {{width: '250px', height: '200px'}}/>
                    </Col>
                    <Col md={9}>
                        <h5>{viewDetail.TITLE}</h5>
                        <p>{viewDetail.DESCRIPTION}</p>
                    </Col>
                </Row>
            </Col>
        </Container>
    )
}
