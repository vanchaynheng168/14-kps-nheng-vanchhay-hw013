// import React, { Component } from 'react'
import {Container, Table, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'

import React from 'react'

export default function GetAPI(props) {
    function convertDate(dateStr) {
        var dateString = dateStr;
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = year + "-" + month + "-" + day;
        return date;
      }
    return (
        <Container>
                <h1 >Artical Management</h1>
                 <Button variant="dark" className="mt-3" onClick={() =>{
                    props.history.push("/add");
                }}>Add New Articale
                </Button>
                <Table striped bordered hover className= "mt-5">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATE DATE</th>
                        <th>IMAGE</th>
                        <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.data.map((data) =>(
                            <tr key={data.ID}>
                            <td>{data.ID}</td>
                            <td style={{width: '200px'}}>{data.TITLE}</td>
                            <td style={{width: '200px'}}>{data.DESCRIPTION}</td>
                            <td style={{width: '150px'}}>{convertDate(data.CREATED_DATE)}</td>
                            <td><img src={data.IMAGE} alt="" style={{width: '100px',height: '100px'}}/></td>
                            <td>
                                <Link to = {`/view/${data.ID}`}><Button variant="primary" >VIEW</Button> </Link>
                                <Link to = {`/edit/${data.ID}`}><Button variant="primary" >EDIT</Button> </Link>
                                <Button variant="danger" onClick={(id)=> props.handleDelete(data.ID)}>DELETE</Button></td>
                        </tr>
                        ))}
                       
                    </tbody>
                </Table>
            </Container>
    )
}
